<?php

class PostsController extends AppController {
    // public $helpers = array('Html', 'Form');

    public function index() 
    {
        $this->set('posts', $this->Post->find('all'));
    }

    public function view($id = null) 
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid Post'));
        }

        $post = $this->Post->findById($id);

        if (!$post) {
            throw new NotFoundException(__('Invalid Post'));
        }

        $this->set('post', $post);
    }

    public function add() 
    {
        if ($this->request->is('post')) {
            $this->Post->create();

            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Flash->error(__('Unable to load your post.'));
        }
    }

    public function edit($id = null) 
    {
       if (!$id) {
           throw new NotFoundException(__('Invalid Post'));
       }

       $post = $this->Post->findById($id);
       if (!$post) {
           throw new NotFoundException(__('Invalid Post'));
       }

       if ($this->request->is(array('post', 'put'))) {
           $this->Post->id = $id;

           if ($this->Post->save($this->request->data)) {
               $this->Flash->success(__('Your Post has been updated'));
               return $this->redirect(array('action' => 'index'));
           }

           $this->Flash->error(__('Unable to update your post'));
       }

       if (!$this->request->data) {
           $this->request->data = $post;
       }
    }

    public function delete($id) 
    {
        if ($this->Post->delete($id)) {
            $this->Session->setFlash('Your post has been deleted');
            $this->redirect(array('action' => 'index'));
        }
    }
}