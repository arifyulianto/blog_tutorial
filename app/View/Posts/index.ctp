<h1>Blog Posts</h1>
<br>
<p><?= $this->Html->link("Tambah Data", array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Body</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <?php foreach ($posts as $post) : ?>
    <tr>
        <td><?= $post['Post']['id'] ?></td>
        <td><?= $post['Post']['title'] ?></td>
        <td><?= $post['Post']['body'] ?></td>
        <td><?= $post['Post']['created']; ?></td>
        <td>
            <?= $this->Html->link('View', array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?> |
            <?= $this->Html->link('Edit', array('controller' => 'posts', 'action' => 'edit', $post['Post']['id'])); ?> |
            <?= $this->Html->link('Delete', array('controller' => 'posts', 'action' => 'delete', $post['Post']['id']), array('confirm' => 'Anda Yakin?')); ?>
        </td>
    </tr>
    <?php endforeach; ?>
    
</table>